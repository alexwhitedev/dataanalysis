from lxml import html
import csv

doc = html.parse('report_201907021220.xls')
tmp = doc.xpath('/html/body/table/tbody/tr')

new_dict = {}
id_driver = ''

for tr in tmp:
    tds = list(tr)
    if tr.get('class') == 'group':
        id_driver = tr.get('data-guid')
    elif tr.get('class') != 'group total':
        id_order = tr.get('data-guid')
        new_dict[id_order] = {
            'call': tds[0].text,
            'number': tds[1].text,
            'order': tds[2].text,
            'payment': tds[5].text,
            'summary': tds[7].text,
            'commission': tds[11].text,
            'conditions': tds[13].text,
            'driver': id_driver
        }
        # print(str(id_order) + ':', new_dict[id_order])

with open('data.csv', 'w') as csv_file:
    writer = csv.writer(csv_file, delimiter=',')
    line = ['id_order', 'call', 'number', 'order', 'payment', 'summary', 'commission', 'conditions', 'driver']
    writer.writerow(line)
    for i in new_dict.keys():
        line = [str(i), str(new_dict[i]['call']), str(new_dict[i]['number']),
                str(new_dict[i]['order']), str(new_dict[i]['payment']), float(new_dict[i]['summary'].replace(",", ".")),
                float(new_dict[i]['commission'].replace("Нет данных", "0.0").replace(",", '.')), str(new_dict[i]['conditions']), str(new_dict[i]['driver'])]
        writer.writerow(line)



