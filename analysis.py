import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')

df = pd.read_csv('data.csv')

# отфильтруйте заказы, оставив только те,   которые имеют ненулевую сумму
df = (df[df["summary"] > 0])

# Подсчитайте, колько было наличных заказов
nal = (df[df["payment"] == 'Наличные'])
print("Наличные заказы", nal)

# Подсчитайте, сколько было безналичных заказов
bnal = (df[df["payment"] == 'Безналичные'])
print("Безналичные заказы", bnal)

# Подсчитайте сумму и комиссии заказов для каждого водителя
tmp = df.groupby("call").sum()

# Сумма:
# sum = [i for i in tmp['summary']]
# Комиссия:s
# com = [i for i in tmp['commission']]
print("Суммы заказов и коммиссий для каждого водителя:")
print(tmp.drop(columns='number'))

plt.hist(tmp["summary"], bins=100, cumulative=0, color="#7663b0")
plt.savefig('noncomulative.png', dpi=300)
plt.clf()

plt.hist(tmp["summary"], bins=100, cumulative=1, color="#539caf")
plt.savefig('comulative.png', dpi=300)
plt.clf()

df["ORDER"] = pd.to_datetime(df['order'], format="%d.%m.%y %H:%M")

grouped_5min = df.resample('5 min', on='ORDER')['id_order'].count()
grouped_5min = df.resample('5 min', on='ORDER')['id_order'].count()
plt.clf()
grouped_5min.plot.bar()
plt.tick_params(axis='x', which='major', labelsize=3)
ax = plt.subplot()
ax.xaxis.get_children()[1].set_size(100)
for label in ax.xaxis.get_ticklabels()[::2]:
    label.set_visible(False)
plt.savefig('5min_grouped.pdf', bbox_inches='tight')
# plt.show()

