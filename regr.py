from lxml import html
import csv
import pandas as pd
import numpy as np
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib
import matplotlib.pyplot as plt
import datetime as dt
matplotlib.use('TkAgg')

doc = html.parse('report_201907021220.xls')
tmp = doc.xpath('/html/body/table/tbody/tr')

new_dict = {}
id_driver = ''

for tr in tmp:
    tds = list(tr)
    if tr.get('class') == 'group':
        id_driver = tr.get('data-guid')
    elif tr.get('class') != 'group total':
        id_order = tr.get('data-guid')
        new_dict[id_order] = {
            'summary': tds[7].text,
            'confirm': tds[17].text,
            'onplace': tds[18].text,
            'called': tds[19].text,
            'onroad': tds[20].text,
            'finish': tds[21].text,
            'payment': int(tds[5].text.replace('Наличные', '1').replace('Безналичные', '2').replace('Корпоративный', '3'))
        }

with open('data2.csv', 'w') as csv_file:
    writer = csv.writer(csv_file, delimiter=',')
    line = ['id_order', 'summary', 'finish', 'payment', 'confirm', 'onplace', 'called', 'onroad']
    writer.writerow(line)
    for i in new_dict.keys():
        line = [str(i), float(new_dict[i]['summary'].replace(",", ".")), str(new_dict[i]['finish']),
                str(new_dict[i]['payment']), str(new_dict[i]['confirm']), str(new_dict[i]['onplace']),
                str(new_dict[i]['called']), str(new_dict[i]['onroad'])]
        writer.writerow(line)


df = pd.read_csv('data2.csv')

# те, которые имеют ненулевую сумму
df = (df[df["summary"] > 0])

df["confirm1"] = pd.to_datetime(df['confirm'], format="%H:%M")
df["onplace1"] = pd.to_datetime(df['onplace'], format="%H:%M")
df["called1"] = pd.to_datetime(df['called'], format="%H:%M")
df["onroad1"] = pd.to_datetime(df['onroad'], format="%H:%M")
df["finish1"] = pd.to_datetime(df['finish'], format="%H:%M")
df['payment1'] = df['payment']
# df['delta'] = df['finish1'] - df['onroad1']
print(df)

df = df.sort_values(by='finish1')
cols = ['summary', 'finish1', 'payment', 'onplace1', 'onroad1', 'called1', 'confirm1']
tmp_df = df[cols]
# print(tmp_df)
# tmp_df = tmp_df.set_index('finish1', append=False)
# tmp_df = tmp_df.index.to_julian_date()
# print(tmp_df)

tmp_df['date_delta'] = (tmp_df['finish1'] - df['finish1'].min()) / np.timedelta64(1, 'D')
tmp_df['onplace_delta'] = (tmp_df['onplace1'] - df['onplace1'].min()) / np.timedelta64(1, 'D')
tmp_df['onroad_delta'] = (tmp_df['onroad1'] - df['onroad1'].min()) / np.timedelta64(1, 'D')
tmp_df['called_delta'] = (tmp_df['called1'] - df['called1'].min()) / np.timedelta64(1, 'D')
tmp_df['confirm_delta'] = (tmp_df['confirm1'] - df['confirm1'].min()) / np.timedelta64(1, 'D')
# print(tmp_df)

# tmp_df.plot()
# plt.show()
# print(df[cols])

print(tmp_df.corr())

x = tmp_df[['date_delta']]
y = tmp_df['summary']

slr = LinearRegression()
slr.fit(x, y)
y_pred = slr.predict(x)
print('Slope: {:.2f}'.format(slr.coef_[0]))
print('Intercept: {:.2f}'.format(slr.intercept_))

print('MSE: {:.3f}'.format(
        mean_squared_error(y, y_pred)))
print('R^2: {:.3f}'.format(
        r2_score(y, y_pred)))

plt.scatter(x, y)
plt.plot(x, slr.predict(x), color='red', linewidth=2)
plt.show()
